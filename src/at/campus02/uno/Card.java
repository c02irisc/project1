package at.campus02.uno;

public class Card {

	// SUPERKLASSE der CARDS

	protected String color;
	protected String cardvalue;
	protected int points;

	public Card(String color, String cardvalue, int points) {
		this.color = color;
		this.cardvalue = cardvalue;
		this.points = points;
	}

	public String getColor() {
		return color;
	}

	public String getCardValue() {
		return cardvalue;
	}

	public int getPoints() {
		return points;
	}

	public boolean match(Card otherCard) {
		if (this.color == otherCard.color) {
			return true;
		} else if (this.cardvalue == otherCard.cardvalue) {
			return true;
		} else if (this.color.equals("  ")) {
			return true;
		} else {
			return false;
		}
	}
	
	//public Card (Ca)

	public String toString() {
		return String.format("[%s %s]", color, cardvalue, points);
	}

}
