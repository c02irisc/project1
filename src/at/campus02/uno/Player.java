package at.campus02.uno;

import java.util.HashSet;

public class Player {

	private String name;
	private boolean bot;

	private HashSet<Card> hand = new HashSet<Card>();

	public Player(String name, boolean bot) {
		this.name = name;
		this.bot = bot;
	}

	public String getName() {
		return name;
	}

	public boolean isBot() {
		return bot;
	}

	public int handSize() {
		return hand.size();
	}

	public void pickup(Card nextcard) {
		hand.add(nextcard);
	}

	public Card chooseCard(Card topCard) {
		if (bot) {
			for (Card comparedCard : hand) {
				if (comparedCard.match(topCard)) {
					hand.remove(comparedCard);
					if (hand.size() == 1) {
						System.out.println(name + " ruft UNO");
					}
					if (hand.size() == 0) {
						System.out.println(name + " hat das Spiel gewonnen.");
					}
					return comparedCard;
				}
			}
		} else {
			/////////////////////////////////////////////////////////////
			// Der !bot_Spieler kann hier in der Console eine Karte
			// aus seinen Handkarten ausw�hlen und ausspielen.
			// Es wird die Eingabe angepasst und gepr�ft ob die Karte
			// matched. Wenn nicht, dann wird eine Fehlermeldung ausgegeben
			// der Vorgang wird wiederholt.
			/////////////////////////////////////////////////////////////
			return null;
		}
		return null;
	}

	public void showHand() {
		System.out.print("\n" + name + "\t");
		for (Card x : hand) {
			System.out.print(x + " ");
		}

	}

}
