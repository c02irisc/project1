package at.campus02.uno;

public class UnoTestApp {

	public static void main(String[] args) {

		UnoGame game1 = new UnoGame();
		System.out.println(game1);
		game1.cardstackSize();

		Player person1 = new Player("Anna", true);
		Player person2 = new Player("Berta", false);
		Player person3 = new Player("Chris", true);
		Player person4 = new Player("Dora", true);

		game1.joinGame(person1);
		game1.joinGame(person2);
		game1.joinGame(person3);
		game1.joinGame(person4);

		game1.shuffle();

		game1.deal();
		System.out.println(game1);
		// game1.dealTopCard();
		person1.showHand();
		person2.showHand();
		person3.showHand();
		person4.showHand();

		System.out.println("\nEs liegt: " + game1.getTopCard());
		game1.cardstackSize();
		game1.dealTopCard();
		game1.cardstackSize();
		System.out.println("\nEs liegt: " + game1.getTopCard());

		// person1.chooseCard(game1.getTopCard());

	}

}
