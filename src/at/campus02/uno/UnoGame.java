package at.campus02.uno;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class UnoGame {

	private ArrayList<Card> cardstack = new ArrayList<Card>();
	private ArrayList<Player> player = new ArrayList<Player>();
	private ArrayList<Card> discardpile = new ArrayList<Card>();

	public UnoGame() {
		ArrayList<String> colors = new ArrayList<String>(Arrays.asList("B", "Y", "G", "R"));
		ArrayList<String> action = new ArrayList<String>(Arrays.asList(">>", "<>", "+2"));
		ArrayList<String> wild = new ArrayList<String>(Arrays.asList("+4", "?"));

		for (int value = 0; value < 10; value++) {
			for (int i = 0; i < 2; i++) {
				if (value == 0) {
					i++;
				}
				for (String color : colors) {
					cardstack.add(new CardNumber(color, Integer.toString(value), value));
				}
			}
		}
		for (int i = 0; i < 2; i++) {
			int points = 20;
			for (String color : colors) {
				for (String input : action) {
					cardstack.add(new CardAction(color, input, points));
				}
			}
		}
		for (int i = 0; i < 4; i++) {
			int points = 50;
			for (String input : wild) {
				cardstack.add(new CardWild(" ", input, points));
			}
		}
	}

	// Überprüfung ob Karten verloren gehen?
	public void cardstackSize() {
		int cardHands = 0;
		for (Player cards : player) {
			cardHands += cards.handSize();
		}
		int total = cardstack.size() + discardpile.size() + cardHands;
		System.out.println(
				"\n// INFO: " + cardstack.size() + " / " + discardpile.size() + " / " + cardHands + " = " + total);
	}

	public void shuffle() {
		Collections.shuffle(cardstack);
	}

	public void deal() {
		for (int i = 0; i < 7; i++) {
			for (Player x : player) {
				Card card = cardstack.remove(0);
				x.pickup(card);
			}
		}
	}

	public boolean isSpecialCard(Card checkCard) {
		String[] specialCard = { ">>", "<>", "+2", "+4", "?" };

		for (String special : specialCard) {
			if (checkCard.getCardValue().equals(special)) {
				return true;
			}
		}
		return false;

	}

	// Erste Karte wird vom cardstack aufgedeckt.
	public void dealTopCard() {
		Card topCard = cardstack.remove(0);
		if (!isSpecialCard(topCard) == true) {
			discardpile.add(0, topCard);
		} else {
			System.out.println("// INFO: Sonderkarte, daher: Neue Karte wird gezogen.");
			discardpile.add(0, topCard);
			dealTopCard();
		}

	}

	public void draw() {
		if (cardstack.isEmpty()) {
			System.out.println("Shuffle Cards");
			// Card topCard =
		}
	}

	public void joinGame(Player newplayer) {
		player.add(newplayer);
	}

	public String toString() {
		return String.format("%s", cardstack);
	}

	public Card getTopCard() {
		if (discardpile.isEmpty()) {
			return null;
		} else {
			return discardpile.get(0);
		}
	}

}
